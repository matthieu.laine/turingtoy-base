from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
	
	# position variable
	f_pos = 0
	i_pos = 0
	# stores in associated variables the key for blank, start states, final states for dict 'machine'
	blank_char = machine["blank"]
	start_state = machine["start state"]
	end_state = machine["final states"]
	i_state = start_state
	transition_table = machine["table"]
	states = machine["table"].keys() # we isolate the states 
	# execution history
	history = []
	# magetic tape definition
	mag_t = []
	mag_t.extend(input_)
	
	for i in range(steps) if steps else iter(int, 1):

		# if the pos goes to 'L' whe have to insert 
		if i_pos < 0:
			mag_t.insert(0, blank_char)
			i_pos = 0
		# if the pos is greater than the lenth of the magnetic tape --> append at the end
		if i_pos >= len(mag_t):
			mag_t.append(blank_char)

		# current symbol to read
		symbol = mag_t[i_pos]

		if i_state in states:
			instructions = transition_table[i_state].get(symbol)

			if instructions is not None:
				history.append({
				    "state": i_state,
				    "reading": symbol,
				    "position": i_pos,
				    "memory": "".join(mag_t),
				    "transition": instructions,
				})


				if instructions == "L":
					i_pos -= 1
				elif instructions == "R":
					i_pos += 1
				else:
					# symbol writing
					if "write" in instructions:
					    mag_t[i_pos] = instructions["write"]
					# moving on the mag tape
					if "R" in instructions:
					    i_pos += 1
					    i_state = instructions["R"]
					if "L" in instructions:
					    i_pos -= 1
					    i_state = instructions["L"]
			else:
				print("no instruction for state {} and symbol {}".format(i_state, symbol))
				break
		else:
			print("current state {} not in the transition table".format(i_state))
			break

		if i_state in end_state:
			# remove useless blanks
			#clean_list = [re.sub(r"^\s+|\s+$", "", r) for r in mag_t]
			return "".join(mag_t).strip(blank_char), history, True
	
	# current state
	return "".join(mag_t), history, False	
